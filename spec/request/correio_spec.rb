require 'spec_helper'

describe 'Correios' do
  describe 'POST /correios' do
    before(:each) { @frete = Correios::Frete::Calculador.new }
    it 'should return sedex infos' do
      services = @frete.calcular(:pac, :sedex)
      expect(services.keys.size).to eq(2)
      expect(services[:sedex].tipo).to eq(:sedex)
    end
  end
end
