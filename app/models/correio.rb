class Correio
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor  :cep_origem, :cep_destino, :peso, :comprimento, :largura, :altura
  
  validates :cep_origem, :cep_destino, :peso, :comprimento, :largura, :altura, :presence => true

  def initialize attributes = {}
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

  def frete! params
    @params = params
    get_frete
  end

  def get_frete
    Correios::Frete::Calculador.new({cep_origem: @params['cep_origem'], cep_destino: @params['cep_destino'], peso: @params['peso'], comprimento: @params['comprimento'], largura: @params['largura'], altura: @params['altura']}).calcular :sedex
  end

  private
  def correio_params
    params.require(:correio).permit(:cep_origem, :cep_destino, :peso, :comprimento, :largura)
  end
end
