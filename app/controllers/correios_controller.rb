class CorreiosController < ApplicationController
  def new
    @correio = Correio.new
  end

  def show
    @correio = Correio.new params['correio']
    if @correio.valid?
      @frete = @correio.frete! params['correio']
    end
  end
end
